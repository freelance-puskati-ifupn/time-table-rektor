<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::middleware(['auth'])->group(function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/listSchedules', 'HomeController@listSchedules')->name('list');
    Route::get('/event', function () {
        return view('events/event_data');
    });

    Route::get('/form', function () {
        return view('events/event_form');
    });

    Route::resource('schedules', 'ScheduleController');
    Route::get('/show_share', 'ScheduleController@show_share');
    Route::get('/update_share/{id}/{status}', 'ScheduleController@update_status');
    Route::resource('events', 'EventController');

    Route::get('/profile', 'ProfileController@index');
    // Route::get('/editProfile', 'ProfileController@edit');
    // Route::patch('/editProfile', 'ProfileController@update');
    Route::get('/changePassword', 'ProfileController@editPassword');
    Route::patch('/changePassword', 'ProfileController@updatePassword');
});
