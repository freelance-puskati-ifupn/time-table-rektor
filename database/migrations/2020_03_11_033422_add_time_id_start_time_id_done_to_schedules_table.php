<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTimeIdStartTimeIdDoneToSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('schedules', function (Blueprint $table) {
            $table->unsignedBigInteger('time_id_start')->after('date_done');
            $table->foreign('time_id_start')->references('id')->on('times');
            $table->unsignedBigInteger('time_id_done')->after('time_id_start');
            $table->foreign('time_id_done')->references('id')->on('times');
            $table->dropColumn('time_start');
            $table->dropColumn('time_done');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('schedules', function (Blueprint $table) {
            $table->dropForeign('schedules_time_id_start_foreign');
            $table->dropForeign('schedules_time_id_done_foreign');
            $table->dropColumn('time_id_start');
            $table->dropColumn('time_id_done');
            $table->string('time_start');
            $table->string('time_done');
        });
    }
}
