<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'role' => 'Ketua Rektor',
        ]);

        Role::create([
            'role' => 'Wakil Rektor 1',
        ]);

        Role::create([
            'role' => 'Wakil Rektor 2',
        ]);

        Role::create([
            'role' => 'Wakil Rektor 3',
        ]);
    }
}
