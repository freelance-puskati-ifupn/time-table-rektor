<?php

use Illuminate\Database\Seeder;
use App\Models\Time;

class TimesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Time::create([
            'time' => '06:00',
        ]);

        Time::create([
            'time' => '06:30',
        ]);

        Time::create([
            'time' => '07:00',
        ]);

        Time::create([
            'time' => '07:30',
        ]);

        Time::create([
            'time' => '08:00',
        ]);

        Time::create([
            'time' => '08:30',
        ]);

        Time::create([
            'time' => '09:00',
        ]);

        Time::create([
            'time' => '09:30',
        ]);

        Time::create([
            'time' => '10:00',
        ]);

        Time::create([
            'time' => '10:30',
        ]);

        Time::create([
            'time' => '11:00',
        ]);

        Time::create([
            'time' => '11:30',
        ]);

        Time::create([
            'time' => '12:00',
        ]);

        Time::create([
            'time' => '12:30',
        ]);

        Time::create([
            'time' => '13:00',
        ]);

        Time::create([
            'time' => '13:30',
        ]);

        Time::create([
            'time' => '14:00',
        ]);

        Time::create([
            'time' => '14:30',
        ]);

        Time::create([
            'time' => '15:00',
        ]);

        Time::create([
            'time' => '15:30',
        ]);

        Time::create([
            'time' => '16:00',
        ]);

        Time::create([
            'time' => '16:30',
        ]);

        Time::create([
            'time' => '17:00',
        ]);

        Time::create([
            'time' => '17:30',
        ]);

        Time::create([
            'time' => '18:00',
        ]);

        Time::create([
            'time' => '18:30',
        ]);

        Time::create([
            'time' => '19:00',
        ]);

        Time::create([
            'time' => '19:30',
        ]);

        Time::create([
            'time' => '20:00',
        ]);

        Time::create([
            'time' => '20:30',
        ]);

        Time::create([
            'time' => '21:00',
        ]);

        Time::create([
            'time' => '21:30',
        ]);

        Time::create([
            'time' => '22:00',
        ]);

        Time::create([
            'time' => '22:30',
        ]);

        Time::create([
            'time' => '23:00',
        ]);

        Time::create([
            'time' => '23:30',
        ]);
    }
}
