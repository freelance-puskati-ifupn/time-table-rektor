<?php

use Illuminate\Database\Seeder;
use App\Models\Day;

class DaysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Day::create([
            'day' => 'Senin',
        ]);

        Day::create([
            'day' => 'Selasa',
        ]);

        Day::create([
            'day' => 'Rabu',
        ]);

        Day::create([
            'day' => 'Kamis',
        ]);

        Day::create([
            'day' => 'Jumat',
        ]);
    }
}
