<?php

use Illuminate\Database\Seeder;
use App\Models\Share;

class SharesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Share::create([
            'host' => '1',
            'assist' => '2',
            'status' => 'Tidak Aktif'
        ]);

        Share::create([
            'host' => '1',
            'assist' => '3',
            'status' => 'Tidak Aktif'
        ]);

        Share::create([
            'host' => '1',
            'assist' => '4',
            'status' => 'Tidak Aktif'
        ]);

        Share::create([
            'host' => '2',
            'assist' => '1',
            'status' => 'Tidak Aktif'
        ]);

        Share::create([
            'host' => '2',
            'assist' => '3',
            'status' => 'Tidak Aktif'
        ]);

        Share::create([
            'host' => '2',
            'assist' => '4',
            'status' => 'Tidak Aktif'
        ]);

        Share::create([
            'host' => '3',
            'assist' => '1',
            'status' => 'Tidak Aktif'
        ]);

        Share::create([
            'host' => '3',
            'assist' => '2',
            'status' => 'Tidak Aktif'
        ]);

        Share::create([
            'host' => '3',
            'assist' => '4',
            'status' => 'Tidak Aktif'
        ]);

        Share::create([
            'host' => '4',
            'assist' => '1',
            'status' => 'Tidak Aktif'
        ]);

        Share::create([
            'host' => '4',
            'assist' => '2',
            'status' => 'Tidak Aktif'
        ]);

        Share::create([
            'host' => '4',
            'assist' => '3',
            'status' => 'Tidak Aktif'
        ]);
    }
}
