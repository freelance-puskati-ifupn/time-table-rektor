<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    // model user hanya menerima satu array
    public function run()
    {
        User::create([
            'fullname' => 'Ketua Rektor',
            'username' => 'ketua_rektor',
            'password' => bcrypt('secret_rektor'),
            'role_id' => 1,
        ]);

        User::create([
            'fullname' => 'Admin Ketua Rektor',
            'username' => 'admin_krektor',
            'password' => bcrypt('admin_secret'),
            'role_id' => 1,
        ]);

        User::create([
            'fullname' => 'Wakil Rektor 1',
            'username' => 'wakil_rektor1',
            'password' => bcrypt('wkrektor1'),
            'role_id' => 2,
        ]);

        User::create([
            'fullname' => 'Admin Wakil Rektor 1',
            'username' => 'admin_wkrektor1',
            'password' => bcrypt('wkwk_admin'),
            'role_id' => 2,
        ]);

        User::create([
            'fullname' => 'Wakil Rektor 2',
            'username' => 'wakil_rektor2',
            'password' => bcrypt('wkrektor2'),
            'role_id' => 3
        ]);

        User::create([
            'fullname' => 'Admin Wakil Rektor 2',
            'username' => 'admin_wkrektor2',
            'password' => bcrypt('wkwk2_admin'),
            'role_id' => 3,
        ]);

        User::create([
            'fullname' => 'Wakil Rektor 3',
            'username' => 'wakil_rektor3',
            'password' => bcrypt('wkrektor3'),
            'role_id' => 4
        ]);

        User::create([
            'fullname' => 'Admin Wakil Rektor 3',
            'username' => 'admin_wkrektor3',
            'password' => bcrypt('wkwk3_admin'),
            'role_id' => 4,
        ]);
    }
}
