<?php

use Illuminate\Database\Seeder;
use App\Models\Repeat;

class RepeatsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Repeat::create([
            'id' => 1,
            'repeat' => 'Single Task'
        ]);

        Repeat::create([
            'id' => 2,
            'repeat' => 'Multi Task'
        ]);

        Repeat::create([
            'repeat' => 'Custom Task'
        ]);
    }
}
