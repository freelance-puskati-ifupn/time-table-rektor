<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Auth;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::user()->id; //digantiti dgn id dari auth
        $user = User::findOrFail($id);
        return view('profile.index', ['user' => $user]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function editPassword()
    {
        return view('profile.editPassword');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Update password
     */
    public function updatePassword(Request $request)
    {
        $message = [
            'old_password.required'  => 'Masukkan Password Anda',
            'old_password.min'  => 'Password sekarang minimal 8 karakter',
            'new_password.required'  => 'Masukkan Password baru',
            'new_password.same'    => 'Password tidak sama',
            'new_password.min'  => 'Password baru minimal 8 karakter',
            'new_password1.required'  => 'Konfirmasi Password baru anda',
            'new_password1.min'  => 'Konfirmasi Password minimal 8 karakter',
        ];
        $request->validate(
            [
                'old_password'  =>  'required|min:8',
                'new_password'  =>  'required|min:8|same:new_password1',
                'new_password1'  =>  'required|min:8',
            ],
            $message
        );
        $old_password = $request->old_password;
        $new_password = $request->new_password;
        $new_password1 = $request->new_password1;
        $id = 1; //diganti id dari session
        $password = User::findOrFail($id);

        // cek apakah current password sama dengan di database
        if (Hash::check($old_password, $password->password) == 1) {
            $password->update([
                'password' => Hash::make($new_password),
            ]);
            return redirect('/changePassword')->with('success', 'password berhasil diubah');
        } else {
            return redirect('/changePassword')->with('danger', 'current password salah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
