<?php

namespace App\Http\Controllers;

use App\Models\Repeat_day;
use App\Models\Schedule;
use Illuminate\Http\Request;

use Auth;
use Carbon\Carbon;
use stdClass;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // mengambil data schedule berdasarkan role
        $schedules = $this->getSchedules();

        return view('dashboard.index', compact('schedules'));
    }

    public function listSchedules()
    {
        echo json_encode($this->getSchedules());
    }

    private function getSchedules()
    {
        $schedule_model = new Schedule();
        $repeat_day = new Repeat_day();
        // Untuk izal untuk funsgi schedule_List() aku ganti whareIn, jadi isinya harus array
        $role_id[] = Auth::user()->role_id;

        $schedules = $schedule_model->schedule_List($role_id);
        $schedule_list = array();
        // dd($schedules);
        foreach ($schedules as $schedule) {
            $schedule_date = $schedule_model->date($schedule->id);

            if ($schedule->repeat_id == 1) {
                $schedule_data = new stdClass();

                $schedule_data->title = $schedule->name;
                $schedule_data->start = $schedule_date["date_start"] . " " . $schedule_date["time_start"];
                $schedule_data->end = $schedule_date["date_done"] . " " . $schedule_date["time_done"];
                $schedule_data->allDay = false;

                $schedule_list[] = $schedule_data;
            } elseif ($schedule->repeat_id == 2) {
                $get_day = $repeat_day->get_by_schedule_id($schedule->id);
                $days = [];
                foreach ($get_day as $day) {
                    $days[] = $day->day_id;
                }

                $date_start = $schedule_date["date_start"] . " " . $schedule_date["time_start"];
                $date_start_end = $schedule_date["date_start"] . " " . $schedule_date["time_done"];
                $date_done = $schedule_date["date_done"] . " " . $schedule_date["time_done"];

                $carbon_start = new Carbon($date_start);
                $carbon_done = new Carbon($date_start_end);
                $diff = intval($carbon_start->diff($date_done)->days);
                for ($i = 0; $i <= $diff; $i++) {
                    $schedule_day_start = $carbon_start->addDays($i ? 1 : 0)->format('Y-m-d H:i:s');
                    $schedule_day_end = $carbon_done->addDays($i ? 1 : 0)->format('Y-m-d H:i:s');

                    if (in_array($carbon_start->format('w'), $days)) {
                        $schedule_data = new stdClass();
                        $schedule_data->title = $schedule->name;
                        $schedule_data->start = $schedule_day_start;
                        $schedule_data->end = $schedule_day_end;
                        $schedule->allDay = false;
                        array_push($schedule_list, $schedule_data);
                    }
                }
            }
        }

        return $schedule_list;
    }
}
