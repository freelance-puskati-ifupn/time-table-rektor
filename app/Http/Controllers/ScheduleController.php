<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\Schedule;
use App\Models\Time;
use App\Models\Share;
use App\Models\Day;
use App\Models\Repeat_day;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use stdClass;

class ScheduleController extends Controller
{
    function __construct()
    {
        $this->schedule = new Schedule;
        $this->share = new Share;
        $this->repeat_day = new Repeat_day;
    }
    /**
     * Menampilkan List Jadwal Kegiatan
     * dikirim ke view event.event_data
     * membawa variabel schedules
     */
    public function index()
    {
        $role_id = Auth::user()->role_id;

        // Mengambil role_id yang memperbolehkan list kegiatannya diliat oleh yang login sekarang
        $shares = $this->share->get_share_by_status($role_id);
        foreach ($shares as $share) {
            $id[] = $share->host;
        }
        $id[] = $role_id; // Memasukkan role_id yang sekarang login

        //Mengambil list kegiatan sesuai list $id[]
        $schedules = $this->schedule->schedule_List($id);
        return view('events.event_data', compact('schedules'));
    }

    /**
     * Fungsi untuk membuka form untuk membuat kegiatan baru
     *
     */
    public function create()
    {
        // Mengambil list waktu
        $times = Time::all();
        $days = Day::all();

        // Mengeset kosong pada nilai yang ada diform
        $schedules = new stdClass();
        $schedules->name = "";
        $schedules->event_id = "";
        $schedules->repeat_id = "";
        $schedules->date_start = "";
        $schedules->date_done = "";
        $schedules->time_id_start = "";
        $schedules->time_id_done = "";
        return view('events.event_form', compact('schedules', 'times', 'days'));
    }

    /**
     * Pemasukkan data baru untuk kegiatan baru dan jadwal baru
     */
    public function store(Request $request)
    {

        // Kondisi untuk tipe repeatnya jika tipe sama dengan single task
        if ($request->repeat == 1) :
            $message = [
                'name.required' => 'Nama kegiatan harus diisi',
                'date_start_single.required' => 'Waktu kegiatan harus diisi',
                'different' => 'Waktu mulai dan selesai tidak bisa sama',
                'time_start_single.lt'  => 'Waktu mulai harus kurang dari waktu selesai',
                'time_done_single.gt'   => 'Waktu selesai harus lebih dari waktu mulai',
            ];
            $request->validate([
                'name'  => 'required',
                'date_start_single'  => 'required',
                'time_start_single' => 'required|different:time_done_single|lt:time_done_single',
                'time_done_single' => 'required|different:time_start_single|gt:time_start_single',
            ], $message);
            $request->date_start = $request->date_start_single;
            $request->date_done = $request->date_start_single;
            $request->time_id_start = $request->time_start_single;
            $request->time_id_done = $request->time_done_single;

            // Pengambilan data jadwal yang kemungkinan tanggal dan waktu sama
            $dataSchedule = $this->schedule->cek_schedule(
                $request->date_start,
                $request->date_done,
                Auth::user()->id,
                $request->time_id_start,
                $request->time_id_done
            );

            // Perulangan untuk pemanggilan data jadwal yang didapat
            foreach ($dataSchedule as $row) {
                // Kondisi Jika data jadwal yang didapat bertipe single task
                if ($row->repeat_id == 1) :
                    // Jika true maka akan langsung dikembalikan ke form
                    return redirect('/schedules/create')->with('failed', 'Maaf sabar 1');

                // Jika false berarti bertipe multi task
                else :
                    // Merubah date_start bertipe date
                    $date = date_create($request->date_start);
                    // Diambil nilai hari berupa angka 0 = Sunday - 6 = Saturday
                    $day = date_format($date, "w");
                    // Cek apakah pada table repeat_day dengan id jadwal sudah ada?
                    $numDay = $this->repeat_day->get_count_schedule($row->id, $day);
                    // Jika lebih dari 1 atau bisa dibilang ada, maka akan dikembalikan ke form
                    if ($numDay > 0) :
                        return redirect('/schedules/create')->with('failed', 'Maaf sabar 2');
                    endif;
                endif;
            }

        // Jika tidak berarti tipenya multi task
        else :
            $message = [
                'name.required' => 'Nama kegiatan harus diisi',
                'date_start_multi.required'  => 'Tanggal mulai harus diisi',
                'date_done_multi.required'  => 'Tanggal selesai harus diisi',
                'time_start_multi.required'  => 'Waktu mulai harus diisi',
                'time_done_multi.required'  => 'Waktu selesai harus diisi',
                'time_start_multi.lt'  => 'Waktu mulai harus kurang dari waktu selesai',
                'time_done_multi.gt'  => 'Waktu selesai harus lebih dari waktu mulai',
                'date_start_multi.before'  => 'Tanggal mulai harus kurang dari tanggal selesai',
                'date_done_multi.after'  => 'Tanggal selesai harus lebih dari tanggal mulai',
                'repeat_day.required_without_all' => 'Hari harus dipilih minimal 1',
            ];
            $request->validate([
                'name'  => 'required',
                'date_start_multi'  => 'required|before:date_done_multi',
                'date_done_multi'   => 'required|after:date_start_multi',
                'time_start_multi'  =>  'required|lt:time_done_multi',
                'time_done_multi'  =>  'required|gt:time_start_multi',
                'repeat_day' => 'required_without_all',
            ], $message);
            $request->date_start = $request->date_start_multi;
            $request->date_done = $request->date_done_multi;
            $request->time_id_start = $request->time_start_multi;
            $request->time_id_done = $request->time_done_multi;

            // Pengambilan data jadwal yang kemungkinan tanggal dan waktu sama
            $dataSchedule = $this->schedule->cek_schedule(
                $request->date_start,
                $request->date_done,
                Auth::user()->id,
                $request->time_id_start,
                $request->time_id_done
            );
            // Perulangan untuk pemanggilan data jadwal yang didapat
            foreach ($dataSchedule as $row) {
                // Kondisi Jika data jadwal yang didapat bertipe single task
                if ($row->repeat_id == 1) :
                    // Merubah date_start bertipe date
                    $date = date_create($row->date_start);
                    // Diambil nilai hari berupa angka 0 = Sunday - 6 = Saturday
                    $day = date_format($date, "w");
                    // Perulangan sesuai jumlah hari yang diinputkan
                    foreach ($request->repeat_day as $repeat_day) {
                        /* 
                        * Kondisi jika hari yang diinputkan sudah terdapat pada database 
                        * sesuai dengan id jadwal yang didapat
                        * maka akan dikembalikan ke form
                        */
                        if ($repeat_day == $day) :
                            return redirect('/schedules/create')->with('failed', 'Maaf sabar 3');
                        endif;
                    }
                // Jika false berarti bertipe multi task
                else :
                    // Perulangan sesuai jumlah hari yang diinputkan
                    foreach ($request->repeat_day as $repeat_day) {
                        // Cek apakah pada table repeat_day dengan id jadwal sudah ada?
                        $numDay = $this->repeat_day->get_count_schedule($row->id, $repeat_day);
                        // Jika lebih dari 1 atau bisa dibilang ada, maka akan dikembalikan ke form
                        if ($numDay > 0) :
                            return redirect('/schedules/create')->with('failed', 'Maaf sabar 4');
                        endif;
                    }
                endif;
            }
        endif;

        // Membuat kegiatan baru pada table event
        $event = Event::create(array('name' => $request->name));

        $data = array(
            'user_id' => Auth::user()->id,
            'event_id' => $event->id,
            'repeat_id' => $request->repeat,
            'date_start' => $request->date_start,
            'date_done' => $request->date_done,
            'time_id_start' => $request->time_id_start,
            'time_id_done' => $request->time_id_done,
            'status' => "Tidak Aktif",
        );

        // Membuat jadwal baru pada table schedule
        $schedule = Schedule::create($data);

        if (!empty($request->repeat_day)) :
            foreach ($request->repeat_day as $row) {
                $dataDay = array(
                    'schedule_id' => $schedule->id,
                    'day_id' => $row
                );

                $repeat_day = Repeat_day::create($dataDay);
            }
        endif;

        return redirect('/schedules')->with('success', 'Data Berhasil Ditambah');
    }

    /**
     * Menampilkan detail kegiatan sesuai dengan kegiatan yang dipilih
     */
    public function show($id)
    {
        $times = Time::all();
        //Mengambil kegiatan berdasarkan id
        $schedules = $this->schedule->schedule_by_id($id);
        $repeat_days = $this->repeat_day->get_by_schedule_id($id);

        $next = '';
        foreach ($repeat_days as $row) {
            $next .= $row->day . ", ";
        }
        $day = $next;
        return view('events.event_detail', compact('schedules', 'times', 'day'));
    }

    /**
     * Membuka form yang berisi nilai kegiatan untuk diupdate
     */
    public function edit($id)
    {

        $times = Time::all();
        $days = Day::all();
        $schedules = $this->schedule->schedule_by_id($id);
        $repeat_days = $this->repeat_day->get_by_schedule_id($id);

        $type = "update";
        return view('events.event_form', compact('schedules', 'type', 'times', 'days', 'repeat_days'));
    }

    /**
     * Mengupdate data kegiatan dan jadwal kegiatan
     */
    public function update(Request $request, $id)
    {

        // Kondisi untuk tipe repeatnya jika tipe sama dengan single task
        if ($request->repeat == 1) :
            $message = [
                'name.required' => 'Nama kegiatan harus diisi',
                'date_start_single.required' => 'Waktu kegiatan harus diisi',
                'different' => 'Waktu mulai dan selesai tidak bisa sama',
                'time_start_single.lt'  => 'Waktu mulai harus kurang dari waktu selesai',
                'time_done_single.gt'   => 'Waktu selesai harus lebih dari waktu mulai',
            ];
            $request->validate([
                'name'  => 'required',
                'date_start_single'  => 'required',
                'time_start_single' => 'required|different:time_done_single|lt:time_done_single',
                'time_done_single' => 'required|different:time_start_single|gt:time_start_single',
            ], $message);
            $request->date_start = $request->date_start_single;
            $request->date_done = $request->date_start_single;
            $request->time_id_start = $request->time_start_single;
            $request->time_id_done = $request->time_done_single;

            // Pengambilan data jadwal yang kemungkinan tanggal dan waktu sama
            $dataSchedule = $this->schedule->cek_schedule(
                $request->date_start,
                $request->date_done,
                Auth::user()->id,
                $request->time_id_start,
                $request->time_id_done
            );

            // Perulangan untuk pemanggilan data jadwal yang didapat
            foreach ($dataSchedule as $row) {
                /* 
                * Cek terlebih dahulu apakah id yang ingin di update 
                * tidak sama dengan id dari data jadwal yang didapat
                */
                if ($row->id != $id) :
                    // Kondisi Jika data jadwal yang didapat bertipe single task
                    if ($row->repeat_id == 1) :
                        // Jika true maka akan langsung dikembalikan ke form
                        return redirect('/schedules/' . $id . '/edit')->with('failed', 'Maaf salah 1');

                    // Jika false berarti bertipe multi task
                    else :
                        // Merubah date_start bertipe date
                        $date = date_create($request->date_start);
                        // Diambil nilai hari berupa angka 0 = Sunday - 6 = Saturday
                        $day = date_format($date, "w");
                        // Cek apakah pada table repeat_day dengan id jadwal sudah ada?
                        $numDay = $this->repeat_day->get_count_schedule($row->id, $day);
                        // Jika lebih dari 1 atau bisa dibilang ada, maka akan dikembalikan ke form
                        if ($numDay > 0) :
                            return redirect('/schedules/' . $id . '/edit')->with('failed', 'Maaf salah 2');
                        endif;
                endif;
                endif;
            }

        // Jika tidak berarti tipenya multi task
        else :
            $message = [
                'name.required' => 'Nama kegiatan harus diisi',
                'date_start_multi.required'  => 'Tanggal mulai harus diisi',
                'date_done_multi.required'  => 'Tanggal selesai harus diisi',
                'time_start_multi.required'  => 'Waktu mulai harus diisi',
                'time_done_multi.required'  => 'Waktu selesai harus diisi',
                'time_start_multi.lt'  => 'Waktu mulai harus kurang dari waktu selesai',
                'time_done_multi.gt'  => 'Waktu selesai harus lebih dari waktu mulai',
                'date_start_multi.before'  => 'Tanggal mulai harus kurang dari tanggal selesai',
                'date_done_multi.after'  => 'Tanggal selesai harus lebih dari tanggal mulai',
                'repeat_day.required_without_all' => 'Hari harus dipilih minimal 1',
            ];
            $request->validate([
                'name'  => 'required',
                'date_start_multi'  => 'required|before:date_done_multi',
                'date_done_multi'   => 'required|after:date_start_multi',
                'time_start_multi'  =>  'required|lt:time_done_multi',
                'time_done_multi'  =>  'required|gt:time_start_multi',
                'repeat_day' => 'required_without_all',
            ], $message);
            $request->date_start = $request->date_start_multi;
            $request->date_done = $request->date_done_multi;
            $request->time_id_start = $request->time_start_multi;
            $request->time_id_done = $request->time_done_multi;

            // Pengambilan data jadwal yang kemungkinan tanggal dan waktu sama
            $dataSchedule = $this->schedule->cek_schedule(
                $request->date_start,
                $request->date_done,
                Auth::user()->id,
                $request->time_id_start,
                $request->time_id_done
            );
            // Perulangan untuk pemanggilan data jadwal yang didapat
            foreach ($dataSchedule as $row) {
                /* 
                * Cek terlebih dahulu apakah id yang ingin di update 
                * tidak sama dengan id dari data jadwal yang didapat
                */
                if ($row->id != $id) :
                    // Kondisi Jika data jadwal yang didapat bertipe single task
                    if ($row->repeat_id == 1) :
                        // Merubah date_start bertipe date
                        $date = date_create($row->date_start);
                        // Diambil nilai hari berupa angka 0 = Sunday - 6 = Saturday
                        $day = date_format($date, "w");
                        // Perulangan sesuai jumlah hari yang diinputkan
                        foreach ($request->repeat_day as $repeat_day) {
                            /* 
                        * Kondisi jika hari yang diinputkan sudah terdapat pada database 
                        * sesuai dengan id jadwal yang didapat
                        * maka akan dikembalikan ke form
                        */
                            if ($repeat_day == $day) :
                                return redirect('/schedules/' . $id . '/edit')->with('failed', 'Maaf salah 3');
                            endif;
                        }
                    // Jika false berarti bertipe multi task
                    else :
                        // Perulangan sesuai jumlah hari yang diinputkan
                        foreach ($request->repeat_day as $repeat_day) {
                            // Cek apakah pada table repeat_day dengan id jadwal sudah ada?
                            $numDay = $this->repeat_day->get_count_schedule($row->id, $repeat_day);
                            // Jika lebih dari 1 atau bisa dibilang ada, maka akan dikembalikan ke form
                            if ($numDay > 0) :
                                return redirect('/schedules/' . $id . '/edit')->with('failed', 'Maaf salah 4');
                            endif;
                        }
                    endif;
                endif;
            }
        endif;

        // Untuk update nama event
        Event::whereId($request->name_id)->update(array('name' => $request->name));

        $updateSchedule = array(
            'repeat_id' => $request->repeat,
            'date_start' => $request->date_start,
            'date_done' => $request->date_done,
            'time_id_start' => $request->time_id_start,
            'time_id_done' => $request->time_id_done,
        );

        // Untuk mengupdate data jadwal
        Schedule::whereId($id)->update($updateSchedule);

        // Konidis untuk update hari yang diulang
        if (!empty($request->repeat_day)) :
            // Pemanggilan repeat_day dengn id jadwal secara descending
            $repeat_days = $this->repeat_day->get_by_schedule_id_desc($id);
            // Menjumlahkan data yang dipanggil tadi
            $Amount_day = count($repeat_days);
            // Menjumlahkan data hari yang diulang yang baru
            $Amount_newday = count($request->repeat_day);

            //Kondisi jika jumlah data yang baru lebih besar dari data yang lama
            if ($Amount_newday > $Amount_day) :
                $repeat = $Amount_newday - $Amount_day;
                for ($i = 0; $i < $repeat; $i++) :
                    $datanewday = array(
                        'schedule_id' => $id,
                        'day_id' => 1
                    );
                    // Jika true maka akan membuat data baru dengan default day_id = 1
                    Repeat_day::create($datanewday);
                endfor;
            // Kondisi jika false maka akan masuk kondisi jika data baru kurang dari data lama
            else : if ($Amount_newday < $Amount_day) :
                    $repeat =  $Amount_day - $Amount_newday;
                    for ($i = 0; $i < $repeat; $i++) :
                        // Jika true maka akan menghapus data yang lama dari data yang terakhir
                        Repeat_day::findOrfail($repeat_days[$i]->id)->delete();
                    endfor;
                endif;
            endif;

            // Pemanggilan hari yang diulang dengan ascending
            $repeat_days = $this->repeat_day->get_by_schedule_id($id);
            $index_repeat_days = 0;

            foreach ($request->repeat_day as $row) {
                // Akan diupdate sesuai dengan id repeat_day yang berada pada $repeat_days
                Repeat_day::whereId($repeat_days[$index_repeat_days]->id)->update(array('day_id' => $row));
                $index_repeat_days++;
            }
        endif;
        return redirect('/schedules')->with('success', 'Data Berhasil di ubah');
    }

    /**
     * Menghapus jadwal kegiatan berdasarkan kegiatan yang dipillih
     */
    public function destroy($id)
    {
        $schedule = Schedule::findOrfail($id);
        $schedule->delete();

        return redirect('/schedules')->with('success', 'Data Telah Dihapus');
    }

    /**
     * Menampilkan table berbagi.
     */
    public function show_share()
    {
        $id = Auth::user()->role_id;
        $shares = $this->share->get_share_by_host($id);
        return view('events.event_share', compact('shares'));
    }

    /**
     * Mengupdate status berbagi aktif/tidak aktif
     */
    public function update_status($id, $status)
    {

        if ($status == "Aktif") :
            $data = array(
                'status' => 'Tidak Aktif'
            );
        else :
            $data = array(
                'status' => 'Aktif'
            );
        endif;

        Share::whereId($id)->update($data);

        return redirect('/show_share')->with('success', 'Data Telah Dihapus');
    }
}
