<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Event extends Model
{
    use Notifiable;

    protected $table = 'events';
    protected $primaryKey = 'id';

    protected $fillable = [
        'name', 'description',
    ];
}
