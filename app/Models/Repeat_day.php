<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Repeat_day extends Model
{
    protected $fillable = [
        'schedule_id', 'day_id',
    ];

    /*
    * untuk mengambil data repeat_day pada table repeat_day
    * berdasarkan schedule_id
    */
    public function get_by_schedule_id($id)
    {
        $query = DB::table('repeat_days')
            ->join('days', 'days.id', '=', 'repeat_days.day_id')
            ->select(
                'repeat_days.id',
                'schedule_id',
                'day_id',
                'repeat_days.created_at',
                'repeat_days.updated_at',
                'day'
            )
            ->where('schedule_id', $id)
            ->orderByRaw('repeat_days.id', 'day_id')
            ->get();
        return $query;
    }

    /*
    * untuk mengambil jumlah repeat_day pada table repeat_day
    * berdasarkan schedule_id dan day_id
    */
    public function cek_day($id, $dayid)
    {
        $query = DB::table('repeat_days')
            ->join('days', 'days.id', '=', 'repeat_days.day_id')
            ->where([
                ['schedule_id', $id],
                ['day_id', $dayid]
            ])
            ->count();
        return $query;
    }

    /*
    * untuk mengambil data repeat_day pada table repeat_day
    * berdasarkan schedule_id 
    * orderby berdasarkan id descending
    */
    public function get_by_schedule_id_desc($id)
    {
        $query = DB::table('repeat_days')
            ->where('schedule_id', $id)
            ->orderByDesc('id')
            ->get();
        return $query;
    }


    /*
    * untuk mengambil total repeat_day pada table repeat_day
    * berdasarkan schedule_id dan day_id 
    */
    public function get_count_schedule($id, $day)
    {
        $query = DB::table('repeat_days')
            ->where([
                ['schedule_id', $id],
                ['day_id', $day]
            ])
            ->count();
        return $query;
    }
}
