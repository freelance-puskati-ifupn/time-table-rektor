<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Schedule extends Model
{
    public $table = 'schedules';
    protected $primaryKey = 'id';

    protected $fillable = [
        'event_id',
        'repeat_id',
        'user_id',
        'date_start',
        'date_done',
        'time_id_start',
        'time_id_done',
        'status',
    ];


    /*
    * Mengambil data jadwal pada table schedule
    * berdasarkan date_start, date_done, role_id, time_id_start, time_id_done
    * guna untuk mendapatkan jadwal yang tabrakan
    */
    public function cek_schedule($date_start, $date_done, $role_id, $time_id_start, $time_id_done)
    {
        $query = DB::select(
            "SELECT
                schedules.id,
                schedules.repeat_id,
                schedules.date_start
        FROM schedules
        JOIN users ON schedules.user_id = users.id
        WHERE ('$date_start' BETWEEN date_start AND date_done OR date_start BETWEEN '$date_start' AND '$date_done')
        AND role_id = $role_id AND (time_id_start BETWEEN $time_id_start AND $time_id_done
        OR $time_id_start BETWEEN time_id_start AND time_id_done)"
        );
        return $query;
    }

    /*
    * Mengambil data jadwal pada table schedule
    * dengan parameter date dan role_id
    * guna untuk mendapatkan jadwal sesuai dengan tanggal dan id role
    */
    public function get_schedule_time($date, $role_id)
    {
        $query = DB::select(

            "SELECT
                schedules.id,
                schedules.repeat_id,
                schedules.time_id_start,
                schedules.time_id_done
            FROM schedules
            JOIN users ON schedules.user_id = users.id
            WHERE '$date' BETWEEN date_start AND date_done AND role_id = $role_id
            ORDER BY schedules.time_id_start"
        );
        return $query;
    }

    /*
    * Mengambil jumlah jadwal waktu yang kosong
    * dengan parameter date dan role_id
    */
    public function get_amount_empty_schedule($date, $role_id)
    {
        $this->repeat_day = new Repeat_day;
        // Merubah date_start bertipe date
        $vdate = date_create($date);
        // Diambil nilai hari berupa angka 0 = Sunday - 6 = Saturday
        $numday = date_format($vdate, "w");
        $schedule = Schedule::get_schedule_time($date, $role_id);
        $subtotal = 0;
        foreach ($schedule as $row) {
            if ($row->repeat_id == 1) :
                $subtotal = $subtotal + $row->time_id_done - $row->time_id_start + 1;
            else :
                $days = $this->repeat_day->cek_day($row->id, $numday);
                if ($days > 0) :
                    $subtotal = $subtotal + $row->time_id_done - $row->time_id_start + 1;
                endif;
            endif;
        }
        $total = 36 - $subtotal;
        return $total;
    }

    /*
    * Mendapatkan waktu yang masih belum diambil
    * dengan parameter date dan role_id
    */
    public function get_empty_time_schedule($date, $role_id)
    {
        $this->repeat_day = new Repeat_day;
        $times = Time::all();
        $schedules = Schedule::get_schedule_time($date, $role_id);
        // Merubah date_start bertipe date
        $vdate = date_create($date);
        // Diambil nilai hari berupa angka 0 = Sunday - 6 = Saturday
        $numday = date_format($vdate, "w");
        $next = '';
        $j = 0;
        for ($i = 0; $i < count($times); $i++) {
            // Cek apakah $schedule[$j] tersebut ada datanya
            if (isset($schedules[$j])) :

                // Jika true cek lagi apakah tipe repeatnya adalah single
                if ($schedules[$j]->repeat_id == 1) :

                    // Jika true cek lagi apakah id pada times[$i] tidak sama dengan id waktu pada schedules[$j]
                    if ($times[$i]->id != $schedules[$j]->time_id_start) :
                        //jika true maka masukkan waktu times[$i] ke variable next
                        $next .= $times[$i]->time;
                        /* Cek apakah pada id times $i+1 tidak sama dengan $schedules[$j]->time_id_start
                         * kondisi ini digunakan untuk memberikan tanda koma langsung
                         * jika id setelahnya itu sama dengan $schedules[$j]->time_id_start
                         */
                        if (($times[$i + 1]->id != $schedules[$j]->time_id_start)) :
                            /* Jika kondisinya true maka akan dilakukan perulangan
                             * Digunakan untuk mencari id yang sama dengan waktu jadwal yang sudah ada
                             * jika ditemukan maka pada string akan berbentuk seperti misal ' - 09.30, '
                             */
                            while ((isset($schedules[$j])) && (isset($times[$i + 1])) && ($i < count($times)) && ($times[$i]->id != $schedules[$j]->time_id_start)) {
                                if ($times[$i + 1]->id == $schedules[$j]->time_id_start) :
                                    $next .= " - " . $times[$i]->time . ", ";
                                    $i = $i + $schedules[$j]->time_id_done - $schedules[$j]->time_id_start + 1;
                                    $j++;
                                else :
                                    $i++;
                                endif;
                            } else :
                            // Jika sama maka akan langsung diberikan tanda koma
                            $next .= ", ";
                            $i = $i + $schedules[$j]->time_id_done - $schedules[$j]->time_id_start + 1;
                            $j++;
                        endif;
                    else :
                        /* Jika tidak sama maka $i akan langsung ditambahkan
                         * dengan jarak waktu yang sudah terdapat jadwal disitu
                         * ini dibuat hanya untuk jika data waktu awal sudah terdapat jadwal yang digunakan
                         */

                        $i = $i + $schedules[$j]->time_id_done - $schedules[$j]->time_id_start;
                        $j++;

                    endif;
                else :
                    $days = $this->repeat_day->cek_day($schedules[$j]->id, $numday);
                    if ($days > 0) :
                        // Jika true cek lagi apakah id pada times[$i] tidak sama dengan id waktu pada schedules[$j]
                        if ($times[$i]->id != $schedules[$j]->time_id_start) :

                            //jika true maka masukkan waktu times[$i] ke variable next
                            $next .= $times[$i]->time;
                            /* Cek apakah pada id times $i+1 tidak sama dengan $schedules[$j]->time_id_start
                             * kondisi ini digunakan untuk memberikan tanda koma langsung
                             * jika id setelahnya itu sama dengan $schedules[$j]->time_id_start
                             */
                            if (($times[$i + 1]->id != $schedules[$j]->time_id_start)) :
                                /* Jika kondisinya true maka akan dilakukan perulangan
                                 * Digunakan untuk mencari id yang sama dengan waktu jadwal yang sudah ada
                                 * jika ditemukan maka pada string akan berbentuk seperti misal ' - 09.30, '
                                 */
                                while ((isset($schedules[$j])) && (isset($times[$i + 1])) && ($i < count($times)) && ($times[$i]->id != $schedules[$j]->time_id_start)) {
                                    if ($times[$i + 1]->id == $schedules[$j]->time_id_start) :
                                        $next .= " - " . $times[$i]->time . ", ";
                                        $i = $i + $schedules[$j]->time_id_done - $schedules[$j]->time_id_start + 1;
                                        $j++;
                                    else :
                                        $i++;
                                    endif;
                                } else :
                                // Jika sama maka akan langsung diberikan tanda koma
                                $next .= ", ";
                                $i = $i + $schedules[$j]->time_id_done - $schedules[$j]->time_id_start + 1;
                                $j++;
                            endif;
                        else :
                            /* Jika tidak sama maka $i akan langsung ditambahkan
                             * dengan jarak waktu yang sudah terdapat jadwal disitu
                             * ini dibuat hanya untuk jika data waktu awal sudah terdapat jadwal yang digunakan
                             */
                            $i = $i + $schedules[$j]->time_id_done - $schedules[$j]->time_id_start;
                            $j++;
                        endif;
                    else :
                        /* Jika pada hari tidak ada yang sama maka index j ditambah 1
                         * Guna untuk memindah posisi jadwal ke index selanjutnya
                         * Dan index i dikurangi 1
                         * Guna untuk jika kembali ke perulangan tidak berubah posisi waktu
                         */
                        $j++;
                        $i--;
                    endif;
                endif;
            else :
                // Jika kosong maka mencetak waktu posisi sekarang $times[$i]
                $next .= $times[$i]->time;
                // Cek apakah $i sekarang tidak sama dengan batas $i
                if ($times[$i] != $times[count($times) - 1]) :
                    // Jika true maka langsung cetak data terakhir pada data times
                    $next .= " - " . $times[count($times) - 1]->time;
                    $i = count($times) - 1;
                endif;
            endif;
        }
        return $next;
    }

    /*
    * Mengambil data jadwal pada table schedule
    * berdasarkan role_id dengan $role_id tidak hanya 1
    */
    public function schedule_List($role_id)
    {
        $query = DB::table('schedules')
            ->join('events', 'events.id', '=', 'schedules.event_id')
            ->join('repeats', 'repeats.id', '=', 'schedules.repeat_id')
            ->join('users', 'users.id', '=', 'schedules.user_id')
            ->join('roles', 'users.role_id', '=', 'roles.id')
            ->select(
                'schedules.id',
                'events.name',
                'users.role_id',
                'roles.role',
                'repeats.repeat',
                'schedules.repeat_id',
                'schedules.date_start',
                'schedules.date_done',
                'schedules.time_id_start',
                'schedules.time_id_done',
                'schedules.status'
            )
            ->whereIn('users.role_id', $role_id)
            ->orderByDesc('schedules.date_start')
            ->get();
        return $query;
    }

    /*
    * Mengambil detail jadwal berdasarkan id
    */
    public function schedule_by_id($id)
    {
        $query = DB::table('schedules')
            ->join('events', 'events.id', '=', 'schedules.event_id')
            ->join('repeats', 'repeats.id', '=', 'schedules.repeat_id')
            ->select(
                'schedules.id',
                'events.name',
                'repeats.repeat',
                'schedules.event_id',
                'schedules.repeat_id',
                'schedules.date_start',
                'schedules.date_done',
                'schedules.time_id_start',
                'schedules.time_id_done',
                'schedules.status'
            )
            ->where('schedules.id', $id)
            ->first();
        return $query;
    }

    public function date($schedule_id)
    {
        $schedule = Schedule::where('id', $schedule_id)->first();
        $time_start = Time::where('id', $schedule->time_id_start)->first();
        $time_done = Time::where('id', $schedule->time_id_done)->first();

        // $date_start = $schedule->date_start . " " . $time_start->time;
        // $date_done = $schedule->date_done . " " . $time_end->time;
        // $diffDay_start = new Carbon($date_start);
        // $diffDay_end = new Carbon($date_done);

        return [
            'date_start' => $schedule->date_start,
            'date_done' => $schedule->date_done,
            'time_start' => $time_start->time,
            'time_done' => $time_done->time
            // 'carbon_start' => $diffDay_start,
            // 'carbon_end' => $diffDay_end,
            // 'diff' => $diffDay_start->diff($date_done)->days
        ];
    }
}
