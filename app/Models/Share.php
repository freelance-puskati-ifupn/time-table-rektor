<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Share extends Model
{
    protected $fillable = [
        'host',
        'assist',
        'status'
    ];

    public function get_share_by_host($id)
    {
        $query = DB::table('shares')
            ->join('roles', 'roles.id', '=', 'shares.assist')
            ->select(
                'shares.id',
                'shares.host',
                'shares.assist',
                'shares.status',
                'roles.role'
            )
            ->where('shares.host', $id)
            ->get();

        return $query;
    }

    public function get_share_by_status($id)
    {
        $query = DB::table('shares')
            ->join('roles', 'roles.id', '=', 'shares.assist')
            ->select(
                'shares.host'
            )
            ->where([
                ['shares.assist', '=', $id],
                ['shares.status', '=', 'Aktif'],
            ])
            ->get();

        return $query;
    }
}
