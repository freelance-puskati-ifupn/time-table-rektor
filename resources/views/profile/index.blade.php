@extends('layouts.main')

@section('title','Profile')
    
@section('content')
<section class="content-header">
        <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            <h1>General Form</h1>
            </div>
            <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a></li>
                <li class="breadcrumb-item active">Profile</li>
            </ol>
            </div>
        </div>
        </div><!-- /.container-fluid -->
    </section>
        <section class="content">
    <div class="container-fluid">
        <!-- general form elements -->
        <div class="card card-default">
        <div class="card-header">
            <h3 class="card-title">Profile Anda</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <div class="card-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label for="fullname">Nama</label>
                        <h6>{{ $user->fullname }}</h6>
                    </div>
                    <div class="col-md-6">
                        <label for="member_since">Member Since</label>
                        <h6>{{ $user->created_at }}</h6>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label for="username">Username</label>
                        <h6>{{ $user->username }}</h6>
                    </div>
                    <div class="col-md-6">
                        <label for="password">Password</label>
                        <div>
                            <a href="{{ url('changePassword') }}" class="btn btn-success"><i class="fas fa-key"></i> Ganti Password</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
            <div class="row">
                
            </div>
            <!-- /.row -->
            </div>
            <!-- /.form group -->

            
        </div>
        <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div><!-- /.container-fluid -->
</section>
@endsection