@extends('layouts.main')

@section('title','Edit Password')

@section('content')
<section class="content-header">
        <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            <h1>Kegiatan</h1>
            </div>
            <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('schedules.index') }}">Profile</a></li>
                <li class="breadcrumb-item active">Change Password</li>
            </ol>
            </div>
        </div>
        </div><!-- /.container-fluid -->
    <div class="content">
        <div class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-6">
                @if (session('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('success') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                @if (session('danger'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{ session('danger') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <form action="{{ url('/changePassword') }}" method="post">
                    @csrf
                    @method('patch')
                    <div class="form-group">
                        <label for="old_password">Password Anda</label>
                    <input type="password" class="form-control @error('old_password') is-invalid @enderror" name="old_password" id="old_password" value="{{ old('old_password') }}" placeholder="Current Password">
                    @error('old_password')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                    </div>
                    <div class="form-group">
                        <label for="new_password">Password Baru</label>
                    <input type="password" class="form-control @error('new_password') is-invalid @enderror" name="new_password" id="new_password" value="{{ old('new_password') }}" placeholder="New Password">
                    @error('new_password')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                    <div class="form-group">
                        <label for="new_password1">Konfirmasi Password</label>
                    <input type="password" class="form-control @error('new_password1') is-invalid @enderror" name="new_password1" id="new_password1" value="{{ old('new_password1') }}" placeholder="Confirm Password">
                    @error('new_password1')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                    </div>
                    <button type="submit" class="btn btn-success px-4 float-right">Change Password</button>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
</section>
@endsection