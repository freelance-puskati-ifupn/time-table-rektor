@extends('layouts.main')
{{-- @extends('layouts.select2_css')
@extends('events.event_setting_js') --}}
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Kegiatan</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="{{ route('schedules.index') }}">Jadwal Kegiatan</a></li>
          <li class="breadcrumb-item active">Detail Kegiatan</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <!-- general form elements -->
    <div class="card card-default">
      <div class="card-header">
        <h3 class="card-title">Detail Kegiatan</h3>
      </div>
      <!-- /.card-header -->
      <!-- form start -->
      <div class="card-body">
        <div class="form-group">
          <label for="name_event">Nama Kegiatan</label>
          <h6>{{ $schedules->name }}</h6>
        </div>
        <div class="form-group">
          <label for="tipe_repeat">Jenis</label>
          <h6>{{ $schedules->repeat }}</h6>
        </div>
        <div class="form-group">
          <div class="row">
            <div class="col-md-6">
              <label for="date_start">Tanggal Mulai</label>
              <h6>{{ $schedules->date_start }}</h6>
            </div>
            <!-- /.col -->
            <div class="col-md-6">
              <label for="date_start">Tanggal Selesai</label>
              <h6>{{ $schedules->date_done }}</h6>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.form group -->

        <div class="form-group">
          <div class="row">
            <div class="col-md-6">
              <label>Waktu Mulai</label>
              @foreach ($times as $time)
              @if($time->id == $schedules->time_id_start)
              <h6>{{ $time->time }}</h6>
              @break
              @endif
              @endforeach
            </div>
            <!-- /.col -->
            <div class="col-md-6">
              <label>Waktu Selesai</label>
              @foreach ($times as $time)
              @if($time->id == $schedules->time_id_done)
              <h6>{{ $time->time }}</h6>
              @break
              @endif
              @endforeach
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.form group -->
        @if ($schedules->repeat == 'Multi Task')
        <div class="form-group">
          <label>Hari diulang yang dipilih</label>
          <h6>{{ $day }}</h6>
        </div>
        <!-- /.form group -->
        @endif
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  </div><!-- /.container-fluid -->
</section>

@endsection