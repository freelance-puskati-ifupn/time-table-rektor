@extends('layouts.main')
@extends('layouts.datatable_css')
@extends('layouts.datatable_js')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Kegiatan</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                    <li class="breadcrumb-item active">Jadwal Kegiatan</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12">
            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-3">
                            <h3 class="card-title">Jadwal Kegiatan</h3>
                        </div>
                        <div class="col-md-5">
                        </div>
                        <div class="col-md-4 text-right">
                            <a href="{{ url('/show_share') }}"><button type="button" class="btn btn-info"><i
                                        class="fas fa-share-alt"></i> Berbagi</button></a>
                            <a href="{{ route('schedules.create') }}"><button type="button" class="btn btn-primary"><i
                                        class="fas fa-plus"></i> Tambah Kegiatan</button></a>
                        </div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped text-center">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Kegiatan</th>
                                <th>Tanggal Mulai</th>
                                <th>Tanggal Selesai</th>
                                <th>Pemilik Kegiatan</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($schedules as $schedule)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $schedule->name}}</td>
                                <td>{{ date('d-F-Y', strtotime($schedule->date_start)) }}</td>
                                <td>{{ date('d-F-Y', strtotime($schedule->date_done)) }}</td>
                                <td>{{ $schedule->role }}</td>
                                <td class="d-flex justify-content-around align-items-center">
                                    <a href="{{ route('schedules.show',$schedule->id)}}" class="btn btn-info"
                                        title="Detail"><i class="far fa-eye"></i></a>
                                    @if(Auth::user()->role_id == $schedule->role_id)
                                    <a href="{{ route('schedules.edit',$schedule->id)}}" class="btn btn-warning"
                                        title="Ubah"><i class="far fa-edit"></i></a>
                                    <form action="{{ route('schedules.destroy',$schedule->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger" title="Hapus"><i
                                                class="far fa-trash-alt"></i></button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
@endsection
