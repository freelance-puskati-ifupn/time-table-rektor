@extends('layouts.main')
@extends('layouts.select2_css')
@extends('events.event_setting_js')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Kegiatan</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="{{ route('schedules.index') }}">Jadwal Kegiatan</a></li>
          <li class="breadcrumb-item active">From Kegiatan</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <!-- general form elements -->
    <div class="card card-default">
      <div class="card-header">
        <h3 class="card-title">Form Kegiatan</h3>
      </div>
      @if(session()->get('failed'))
      <div class="alert alert-danger"><i class="fa fa-ban"></i>
        {{ session()->get('failed') }}
      </div>
      @endif
      <!-- /.card-header -->
      <!-- form start -->
      @if (empty($type) )
      <form role="form" method="post" action="{{ route('schedules.store')}}">
        @csrf
        @else
        <form role="form" method="post" action="{{ route('schedules.update', $schedules->id) }}">
          @csrf
          @method('PUT')
          @endif
          <div class="card-body">
            <div class="form-group">
              <label for="name_event">Nama Kegiatan</label>
              <input type="text" name="name_id" value="{{ $schedules->event_id }}" hidden>
              @if (empty($type))
              <input type="text" id="name_event" class="form-control @error('name') is-invalid @enderror" name="name"
                placeholder="Masukkan Nama Kegiatan" value="{{ old('name') }}">
              @else
              <input type="text" id="name_event" class="form-control @error('name') is-invalid @enderror" name="name"
                placeholder="Masukkan Nama Kegiatan" value="{{ $schedules->name }}">
              @endif
              @error('name')
              <div class="invalid-feedback">{{ $message }}</div>
              @enderror
            </div>
            <div class="form-group">
              <label for="tipe_repeat">Jenis</label>
              <select class="form-control select2 @error('repeat') is-invalid @enderror" id="tipe_repeat"
                data="{{ $schedules->repeat_id }}" name="repeat" style="width: 100%;">
                @if ($schedules->repeat_id == 2)
                <option value="1" {{ (1 == old('repeat'))?'selected':'' }}>Single Task</option>
                <option selected="selected" value="2" {{ (2 == old('repeat'))?'selected':'' }}>Multi Task</option>
                @else
                <option selected="selected" value="1" {{ (1 == old('repeat'))?'selected':'' }}>Single Task</option>
                <option value="2" {{ (2 == old('repeat'))?'selected':'' }}>Multi Task</option>
                @endif
              </select>
              @error('repeat')
              <div class="invalid-feedback">{{ $message }}</div>
              @enderror
            </div>

            <!-- /.single -->
            <div id="tipe1">
              <div class="form-group">
                <label for="date_start_single">Tanggal</label>
                <div class="input-group">
                  @if (empty($type))
                  <input type="date" name="date_start_single" id="date_start_single"
                    class="form-control float-right @error('date_start_single') is-invalid @enderror"
                    value="{{ old('date_start_single') }}">
                  @else
                  <input type="date" name="date_start_single" id="date_start_single"
                    class="form-control float-right @error('date_start_single') is-invalid @enderror"
                    value="{{ $schedules->date_start }}">
                  @endif
                  @error('date_start_single')
                  <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

              <div class="form-group">
                <div class="row">
                  <div class="col-md-6">
                    <label>Waktu Mulai</label>
                    <div class="input-group">
                      <select name="time_start_single"
                        class="select2 form-control @error('time_start_single') is-invalid @enderror">
                        <option value="" disabled>Pilih Waktu...</option>
                        @foreach ($times as $time)
                        <option @if($time->id == $schedules->time_id_start) selected @endif
                          value="{{ $time->id }}"
                          {{ ($time->id == old('time_start_single'))?'selected':'' }}>{{ $time->time }}
                        </option>
                        @endforeach
                      </select>
                      @error('time_start_single')
                      <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>
                    <!-- /.input group -->
                  </div>
                  <!-- /.col -->
                  <div class="col-md-6">
                    <label>Waktu Selesai</label>
                    <div class="input-group">
                      <select name="time_done_single"
                        class="select2 form-control @error('time_done_single') is-invalid @enderror">
                        <option value="" disabled>Pilih Waktu...</option>
                        @foreach ($times as $time)
                        <option @if($time->id == $schedules->time_id_done) selected @endif
                          value="{{ $time->id }}"
                          {{ ($time->id == old('time_done_single'))?'selected':'' }}>{{ $time->time }}</option>
                        @endforeach
                      </select>
                      @error('time_done_single')
                      <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>
                    <!-- /.input group -->
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- /.form group -->
            </div>
            <!-- /.tipe1 -->

            <!-- /.multi -->
            <div id="tipe2">
              <div class="form-group">
                <div class="row">
                  <div class="col-md-6">
                    <label for="date_start">Tanggal Mulai</label>
                    <div class="input-group">
                      @if (empty($type))
                      <input type="date" id="date_start" name="date_start_multi"
                        class="form-control float-right @error('date_start_multi') is-invalid @enderror"
                        value="{{ old('date_start_multi') }}">
                      @else
                      <input type="date" id="date_start" name="date_start_multi"
                        class="form-control float-right @error('date_start_multi') is-invalid @enderror"
                        value="{{ $schedules->date_start }}">
                      @endif
                      @error('date_start_multi')
                      <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>
                    <!-- /.input group -->
                  </div>
                  <!-- /.col -->
                  <div class="col-md-6">
                    <label for="date_done">Tanggal Selesai</label>
                    <div class="input-group">
                      @if (empty($type))
                      <input type="date" id="date_done" name="date_done_multi"
                        class="form-control float-right @error('date_done_multi') is-invalid @enderror"
                        value="{{ old('date_done_multi') }}">
                      @else
                      <input type="date" id="date_done" name="date_done_multi"
                        class="form-control float-right @error('date_done_multi') is-invalid @enderror"
                        value="{{ $schedules->date_done}}">
                      @endif
                      @error('date_done_multi')
                      <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>
                    <!-- /.input group -->
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- /.form group -->

              <div class=" form-group">
                <div class="row">
                  <div class="col-md-6">
                    <label for="time_start">Waktu Mulai</label>
                    <div class="input-group">
                      <select name="time_start_multi" id="time_start"
                        class="select2 form-control @error('time_start_multi') is-invalid @enderror">
                        <option value="" disabled>Pilih Waktu...</option>
                        @foreach ($times as $time)
                        <option @if($time->id == $schedules->time_id_start) selected @endif
                          value="{{ $time->id }}"
                          {{ ($time->id == old('time_start_multi'))?'selected':'' }}>{{ $time->time }}</option>
                        @endforeach
                      </select>
                      @error('time_start_multi')
                      <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>
                    <!-- /.input group -->
                  </div>
                  <!-- /.col -->
                  <div class="col-md-6">
                    <label for="time_done">Waktu Selesai</label>
                    <div class="input-group">
                      <select name="time_done_multi" id="time_done"
                        class="select2 form-control @error('time_done_multi') is-invalid @enderror">
                        <option value="" disabled>Pilih Waktu...</option>
                        @foreach ($times as $time)
                        <option @if($time->id == $schedules->time_id_done) selected @endif
                          value="{{ $time->id }}"
                          {{ ($time->id == old('time_done_multi'))?'selected':'' }}>{{ $time->time }}</option>
                        @endforeach
                      </select>
                      @error('time_done_multi')
                      <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>
                    <!-- /.input group -->
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- /.form group -->

              <div class="form-group">
                <label for="day_repeat">Hari yang diulang</label>
                <div class="d-flex">
                    <div class="form-check mr-3 icheck-primary d-inline">
                        <input type="checkbox" id="every_day" value=""/>
                        <label for="every_day">Semua Hari</label>
                    </div>
                    <?php $i = 0 ?>
                    @foreach ($days as $day)
                        <div class="form-check mr-3 icheck-primary d-inline">
                            <input @if(isset($repeat_days[$i]) && ($repeat_days[$i]->day_id == $day->id)) {{ $i++ }} checked
                            @endif
                            name="repeat_day[]" type="checkbox"
                            id="{{ $day->day }}" value="{{ $day->id }}">
                            <label for="{{ $day->day }}">{{ $day->day }}
                            </label>
                        </div>
                    @endforeach
                </div>
                <!-- /.d-flex -->
                @error('repeat_day')
                <div style="font-size: 80%; color: #dc3545;">{{ $message }}</div>
                @enderror
              </div>
              <!-- /.form group -->

            </div>
            <!-- /.tipe2 -->
          </div>
          <!-- /.card-body -->
          <div class="card-footer float-right">
            <button type="submit" class="btn btn-success px-4">Submit</button>
          </div>
        </form>
    </div>
    <!-- /.card -->
  </div><!-- /.container-fluid -->
</section>

@endsection
