@extends('layouts.select2_js')
@section('add_js')
<script>
    $(document).ready(function(){

    var type = $("#tipe_repeat"). val();
    if(type == '2'){
        $("#tipe1").hide();
        $("#tipe2").show();
    }else if(type == '1'){
        $("#tipe2").hide();
        $("#tipe1").show();
    }
    // Untuk mengganti tampilan saat select option jenis kegiatan terganti
    // Pada Tampilan event_form.blade.php
    $('#tipe_repeat').on('change', function(){
        var divisiId = $(this). children("option:selected"). val();

        if(divisiId == '2'){
            $("#tipe1").hide();
            $("#tipe2").show();
        }
        else if(divisiId == '1'){
            $("#tipe2").hide();
            $("#tipe1").show();
        }
    });

    // menceklis seluruh hari
    $('#every_day').click(function() {
        $('input:checkbox').not(this).prop('checked', this.checked);
    });
});
</script>
@endsection
